use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct DotProductRequest {
    numbers1: Vec<i32>,
    numbers2: Vec<i32>,
}

#[derive(Serialize)]
struct DotProductResponse {
    dot_product: Option<i32>,
    error: Option<String>,
}

// Endpoint to find the dot product of two lists of numbers in a JSON request
async fn find_dot_product(item: web::Json<DotProductRequest>) -> impl Responder {
    if item.numbers1.len() != item.numbers2.len() {
        return web::Json(DotProductResponse {
            dot_product: None,
            error: Some("Please enter two lists of numbers of the same length.".to_string()),
        });
    }
    let dot_product = item
        .numbers1
        .iter()
        .zip(item.numbers2.iter())
        .map(|(a, b)| a * b)
        .sum();
    web::Json(DotProductResponse {
        dot_product: Some(dot_product),
        error: None,
    })
}

// Endpoint to serve the HTML page
async fn index() -> impl Responder {
    let html = r#"
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Dot Product Calculator</title>
        <script>
            async function calculateDotProduct() {
                const numbers1 = document.getElementById('numbers1').value.split(',').map(Number);
                const numbers2 = document.getElementById('numbers2').value.split(',').map(Number);
                const response = await fetch('/dot_product', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ numbers1, numbers2 })
                });
                const result = await response.json();
                if (result.error) {
                    document.getElementById('result').textContent = result.error;
                } else {
                    document.getElementById('result').textContent = 'Dot product: ' + result.dot_product;
                }
            }
        </script>
    </head>
    <body>
        <h1>Dot Product Calculator</h1>
        <h3>Instructions</h3>
        <p>1. Please enter lists of numbers separated by a comma.<br>
        2. The 2 lists should be of the same size to calculate the dot product. <br>
        3. Click on "Calculate" to display the dot product of the 2 list of numbers</p>
        <label for="numbers1">Enter the first list of numbers:</label><br>
        <input type="text" id="numbers1" name="numbers1" placeholder="e.g., 1,2,3"><br>
        <label for="numbers2">Enter the second list of numbers:</label><br>
        <input type="text" id="numbers2" name="numbers2" placeholder="e.g., 4,5,6"><br>
        <button onclick="calculateDotProduct()">Calculate</button>
        <p id="result"></p>
    </body>
    </html>
"#;
    HttpResponse::Ok().content_type("text/html").body(html)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/dot_product", web::post().to(find_dot_product))
    })
    .bind("0.0.0.0:8081")?
    .run()
    .await
}

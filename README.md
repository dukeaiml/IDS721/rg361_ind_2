# Rust Web Service

[![pipeline status](https://gitlab.com/dukeaiml/IDS721/rg361_ind_2/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/rg361_ind_2/-/commits/main)

## 1. Overview
In this project, a simple web service is created using ``Rust`` and ``Actix-web``. The webservice is containerized using ``Docker``, and  a ``CICD`` pipeline is implemented in gitlab which is automatically triggered on any updates to the repository.

Please watch this [video](https://youtu.be/blnItdsz2Lw) for a demo and explanation of the project.

## 2. Project Development

### 2.1 Pre-requisites
- install [Rust](https://www.rust-lang.org/tools/install)
- install [Docker](https://docs.docker.com/get-docker/)

### 2.2 Rust Project
- Create a new Rust project using the following command:
```bash 
cargo new <project_name>
```
- write the code for the web service in the ``src/main.rs`` file. refer to the code in this repository for an example.

- add the required dependencies in the ``Cargo.toml`` file.

- build the project using the following command:
```bash
cargo build
```

- run and test the project using the following command:
```bash
cargo run
```
![local-run](./resources/cargo_run.png)  

At this stage, the web service should be running on which ever port you have specified in the code.
in this case, the web service is running on port 8081. you can access it by clicking on the following link: [http://localhost:8081/](http://localhost:8081/)

refer to the [Sample Execution](#3-sample-execution) section for the results.

### 2.3 Containerization

Now that the webservice has been created and tested, the next step is to containerize the application using Docker.

- initialize docker in the project directory using the following command:
```bash
docker init
```
- enter the details as per your requirements:  
![docker-init](./resources/docker_init.png)

- This should have created the required files for docker in the project directory such as ``Dockerfile``, ``.dockerignore`` etc.
- make changes to the ``Dockerfile`` as per the requirements of the project
- build the docker image using the following command:
```bash
docker build -t <image_name> .
```

- run the docker container to test the containerized application:
![docker-run](./resources/docker_run.png)

alternatively, you can run the container using the following command:
```bash
docker run -p 8081:8081 <image_name>
```

**Note**: the port number should be the same as the one specified in the code.

At this stage, the containerized application should be running on which ever port you have specified in the code.
The behavior should be the same as the non-containerized application process mentioned above.

## 3. Sample Execution

The app takes in 2 list of numbers separated by commas, and it returs the dot product of the two lists.  
In case the user enters invalid input, the app returns an error message.

Correct input:  
![correct-input](./resources/local_run.png)
  
Incorrect input:  
![incorrect-input](./resources/local_error.png)

## 4. CICD Pipeline
A CICD pipeline is implemented in gitlab which is automatically triggered on any updates to the repository.
this ensures that the code is always linted, formatted, deployed etc every time a change is made to the repository.

![pipeline](./resources/cicd.png)